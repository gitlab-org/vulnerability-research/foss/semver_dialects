# frozen_string_literal: true

RSpec.describe SemverDialects::IntervalParser do # rubocop:todo Metrics/BlockLength
  let(:typ) { 'npm' }

  context 'Basic functionality when' do
    it 'running on a simple example' do
      expect(described_class.parse(typ, '<=3.2.9').to_s).to eq('(-inf,3.2.9]')
      expect(described_class.parse(typ, '>=2.3.0 <2.3.2').to_s).to eq('[2.3.0,2.3.2)')
      expect(described_class.parse(typ, '>=3.4.0-alpha0 <3.4.1').to_s).to eq('[3.4.0-alpha0,3.4.1)')
    end

    it 'running on a more complex example' do
      version_array = [
        '<=3.2.9',
        '>=4.1-alpha0 <=4.1.3',
        '>=4.2-alpha0 <=4.2.0',
        '<6.6.6',
        '>=7.0.0 <7.7.0'
      ]

      version_array = version_array.map { |item| described_class.parse(typ, item).to_s }

      expect(version_array).to eq(['(-inf,3.2.9]', '[4.1-alpha0,4.1.3]', '[4.2-alpha0,4.2.0]', '(-inf,6.6.6)',
                                   '[7.0.0,7.7.0)'])
    end

    it 'to text conversion' do
      expect(described_class.parse(typ, '<=3.2.9').to_description_s).to eq('all versions up to 3.2.9')
      expect(described_class.parse(typ, '>=2.3.0 <2.3.2').to_description_s)
        .to eq('all versions starting from 2.3.0 before 2.3.2')
      expect(described_class.parse(typ, '>3.4.0-alpha0 <3.4.1').to_description_s)
        .to eq('all versions after 3.4.0-alpha0 before 3.4.1')
      expect(described_class.parse(typ,
                                   '>=3.4.0-alpha0').to_description_s).to eq('all versions starting from 3.4.0-alpha0')
      expect(described_class.parse(typ, '>3.4.0-alpha0').to_description_s).to eq('all versions after 3.4.0-alpha0')
      expect(described_class.parse(typ,
                                   '>=0.0.0-alpha').to_description_s).to eq('all versions starting from 0.0.0-alpha')
    end
  end

  context 'with maven syntax' do
    let(:typ) { 'maven' }

    # Test to demonstrate parsing error, see https://gitlab.com/gitlab-org/gitlab/-/issues/428251 for details.
    it 'raises when attempting to parse a string with newlines' do
      expect { described_class.parse(typ, "1.0\n2.0") }
        .to raise_error(SemverDialects::InvalidConstraintError, "invalid constraint '1.0\n2.0'")
    end

    it 'parses intervals' do
      source_versions = [
        '(,3.15-beta-2]',
        '(,3.2.9],[4.1-alpha-0,4.1.3],[4.2-alpha-0,4.2.0]',
        '(,3.3.10)',
        '(,3.3.7]',
        '(,3.4.14),[3.5.0-alpha,3.5.4-beta]',
        '(,3.8.0)',
        '(,4.2.13)',
        '(,4.3.0]',
        '(,4.5.1]',
        '(,5.1.40]',
        '(,6.6.1)',
        '(,6.6.3),[7,7.3.0)',
        '(,6.6.6),[7.0.0,7.7.0)',
        '(,9.2.27),[9.3.0-milestone-0,9.3.26),[9.4.0.M0,9.4.16)',
        'r80',
        '(,r80)',
        '3.2.0-final',
        '4.0.0-rc1',
        '5.0.0-SNAPSHOT',
        '1.0.0.0',
        'cci.20210118'
      ]

      expected_versions = [
        '(-inf,3.15-beta-2]',
        '(-inf,3.2.9]',
        '[4.1-alpha-0,4.1.3]',
        '[4.2-alpha-0,4.2.0]',
        '(-inf,3.3.10)',
        '(-inf,3.3.7]',
        '(-inf,3.4.14)',
        '[3.5.0-alpha,3.5.4-beta]',
        '(-inf,3.8.0)',
        '(-inf,4.2.13)',
        '(-inf,4.3.0]',
        '(-inf,4.5.1]',
        '(-inf,5.1.40]',
        '(-inf,6.6.1)',
        '(-inf,6.6.3)',
        '[7,7.3.0)',
        '(-inf,6.6.6)',
        '[7.0.0,7.7.0)',
        '(-inf,9.2.27)',
        '[9.3.0-milestone-0,9.3.26)',
        '[9.4.0-milestone-0,9.4.16)',
        '[-r-80,+inf)',
        '(-inf,-r-80)',
        '[3.2.0,+inf)',
        '[4.0.0-rc-1,+inf)',
        '[5.0.0-snapshot,+inf)',
        '[1.0.0.0,+inf)',
        '[-cci.20210118,+inf)'
      ]

      target_versions = source_versions.flat_map do |item|
        SemverDialects::IntervalSetParser.translate_maven(item).map do |version_string|
          described_class.parse(typ, version_string).to_s
        end
      end

      expect(target_versions.to_set).to eq(expected_versions.to_set)
    end

    it 'parses intervals' do
      source_versions = [
        '(,3.15-beta-2]',
        '(,3.2.9]',
        '[4.1-alpha-0,4.1.3]',
        '[4.2-alpha-0,4.2.0]',
        '(,3.3.10)',
        '(,3.3.7]',
        '(,3.4.14)',
        '[3.5.0-alpha,3.5.4-beta]',
        '(,3.8.0)',
        '(,4.2.13)',
        '(,4.3.0]',
        '(,4.5.1]',
        '(,5.1.40]',
        '(,6.6.1)',
        '(,6.6.3)',
        '[7,7.3.0)',
        '(,6.6.6)',
        '[7.0.0,7.7.0)',
        '(,9.2.27)',
        '[9.3.0-milestone-0,9.3.26)',
        '[9.4.0-milestone-0,9.4.16)',
        '[9.4.0-milestone-0]'
      ]

      target_versions = source_versions.flat_map do |item|
        SemverDialects::IntervalSetParser.translate_maven(item).map do |version_string|
          described_class.parse(typ, version_string).to_maven_s
        end
      end

      expect(target_versions.to_set).to eq(source_versions.to_set)
    end
  end

  context 'with nuget syntax' do
    let(:typ) { 'nuget' }

    it 'parses intervals' do
      source_versions = [
        '(,3.15-beta2]',
        '(,3.2.9],[4.1-alpha0,4.1.3],[4.2-alpha0,4.2.0]',
        '(,3.3.10)',
        '(,3.3.7]',
        '(,3.4.14),[3.5.0-alpha,3.5.4-beta]',
        '(,3.8.0)',
        '(,4.2.13)',
        '(,4.3.0]',
        '(,4.5.1]',
        '(,5.1.40]',
        '(,6.6.1)',
        '(,6.6.3),[7,7.3.0)',
        '(,6.6.6),[7.0.0,7.7.0)',
        '(,9.2.27),[9.3.0,9.3.26),[9.4.0,9.4.16)'
      ]

      expected_versions = [
        '(-inf,3.15-beta2]',
        '(-inf,3.2.9]',
        '[4.1-alpha0,4.1.3]',
        '[4.2-alpha0,4.2.0]',
        '(-inf,3.3.10)',
        '(-inf,3.3.7]',
        '(-inf,3.4.14)',
        '[3.5.0-alpha,3.5.4-beta]',
        '(-inf,3.8.0)',
        '(-inf,4.2.13)',
        '(-inf,4.3.0]',
        '(-inf,4.5.1]',
        '(-inf,5.1.40]',
        '(-inf,6.6.1)',
        '(-inf,6.6.3)',
        '[7,7.3.0)',
        '(-inf,6.6.6)',
        '[7.0.0,7.7.0)',
        '(-inf,9.2.27)',
        '[9.3.0,9.3.26)',
        '[9.4.0,9.4.16)'
      ]

      target_versions = source_versions.flat_map do |item|
        SemverDialects::IntervalSetParser.translate_nuget(item).map do |version_string|
          described_class.parse(typ, version_string).to_s
        end
      end

      expect(target_versions.to_set).to eq(expected_versions.to_set)
    end

    it 'parses intervals' do
      source_versions = [
        '(,3.15-beta2]',
        '(,3.2.9]',
        '[4.1-alpha0,4.1.3]',
        '[4.2-alpha0,4.2.0]',
        '(,3.3.10)',
        '(,3.3.7]',
        '(,3.4.14)',
        '[3.5.0-alpha,3.5.4-beta]',
        '(,3.8.0)',
        '(,4.2.13)',
        '(,4.3.0]',
        '(,4.5.1]',
        '(,5.1.40]',
        '(,6.6.1)',
        '(,6.6.3)',
        '[7,7.3.0)',
        '(,6.6.6)',
        '[7.0.0,7.7.0)',
        '(,9.2.27)',
        '[9.3.0,9.3.26)',
        '[9.4.0,9.4.16)',
        '[9.4.0]'
      ]

      target_versions = source_versions.flat_map do |item|
        SemverDialects::IntervalSetParser.translate_nuget(item).map do |version_string|
          described_class.parse(typ, version_string).to_maven_s
        end
      end

      expect(target_versions.to_set).to eq(source_versions.to_set)
    end
  end

  context 'with gem syntax' do
    let(:typ) { 'gem' }

    it 'parses intervals' do
      source_versions = [
        '>=2.0.0.rc3 <2.0.2',
        '>=2.0.2 <=3.1.1',
        '>=2.0.4 <4.0.0.beta1',
        '>=2.0.x <2.0.1.rc1',
        '<1.5.4',
        '>=2.1.0 <4.2.6',
        '>=2.3.0 <3.9.5',
        '>=3.10 <3.12',
        '>=4.0.0.preview2 <4.0.0.rc.2',
        '>=2.3.0.alpha0 <2.3.13'
      ]

      target_versions = source_versions.flat_map do |item|
        SemverDialects::IntervalSetParser.translate_gem(item).map do |version_string|
          described_class.parse(typ, version_string).to_ruby_s
        end
      end

      expect(target_versions.to_set).to eq(source_versions.to_set)
    end

    it 'parses intervals' do
      source_versions = [
        '>=2.0.0 <2.0.13||>=2.1.0 <2.1.11||>=2.2.0 <2.2.5||>=2.3.0 <2.3.10||>=2.4.0',
        '>=2.0.0 <2.2.5 || >=3.0.0 <3.0.1',
        '>=2.0.0 <3.2.16||>=4.0.0.beta1 <4.0.2',
        '>=2.0.0.rc3 <2.0.2',
        '>=2.0.2 <=3.1.1',
        '>=2.0.4 <4.0.0.beta1',
        '>=2.0.x <2.0.1.rc1||<1.5.4',
        '>=2.1.0 <4.2.6',
        '>=2.3.0 <3.9.5 || >=3.10 <3.12 || >=4.0.0.preview2 <4.0.0.rc.2',
        '>=2.3.0.alpha0 <2.3.13'
      ]

      expected_versions = [
        '[2.0.0,2.0.13)',
        '[2.1.0,2.1.11)',
        '[2.2.0,2.2.5)',
        '[2.3.0,2.3.10)',
        '[2.4.0,+inf)',
        '[2.0.0,2.2.5)',
        '[3.0.0,3.0.1)',
        '[2.0.0,3.2.16)',
        '[4.0.0.beta1,4.0.2)',
        '[2.0.0.rc3,2.0.2)',
        '[2.0.2,3.1.1]',
        '[2.0.4,4.0.0.beta1)',
        '[2.0.x,2.0.1.rc1)',
        '(-inf,1.5.4)',
        '[2.1.0,4.2.6)',
        '[2.3.0,3.9.5)',
        '[3.10,3.12)',
        '[4.0.0.preview2,4.0.0.rc.2)',
        '[2.3.0.alpha0,2.3.13)'
      ]

      target_versions = source_versions.flat_map do |item|
        SemverDialects::IntervalSetParser.translate_gem(item).map do |version_string|
          described_class.parse(typ, version_string).to_s
        end
      end

      expect(target_versions.to_set).to eq(expected_versions.to_set)
    end
  end

  context 'with npm syntax' do
    let(:typ) { 'npm' }

    it 'parses intervals' do
      source_versions = [
        '<2.1.5-M1 >=2.1.4-M1',
        '<2.19.3',
        '<2.2.0',
        '<2.4.2',
        '<2.4.24',
        '<2.6.0',
        '<2.6.9 || >= 3.0.0 <3.1.0',
        '<2.9.4',
        '<3.0.0',
        '<3.0.1',
        '<3.11.0 || >=4.0.0-rc1 <4.5.0',
        '<3.23.5',
        '<3.3.1',
        '<3.4.6 || >=4.0.0 <4.0.5',
        '<3.5.0',
        '<3.5.1 || >=4.0.0 && <4.1.3 || >=5.0.0 && <5.6.1 || >=6.0.0 && <6.1.2',
        '<3.7.0',
        '<4.0.0',
        '<4.17.5',
        '<4.2.1 || >= 5.0.0-alpha0 < 5.0.3'
      ]

      expected_versions = [
        '[2.1.4-M1,2.1.5-M1)',
        '(-inf,2.19.3)',
        '(-inf,2.2.0)',
        '(-inf,2.4.2)',
        '(-inf,2.4.24)',
        '(-inf,2.6.0)',
        '(-inf,2.6.9)',
        '[3.0.0,3.1.0)',
        '(-inf,2.9.4)',
        '(-inf,3.0.0)',
        '(-inf,3.0.1)',
        '(-inf,3.11.0)',
        '[4.0.0-rc1,4.5.0)',
        '(-inf,3.23.5)',
        '(-inf,3.3.1)',
        '(-inf,3.4.6)',
        '[4.0.0,4.0.5)',
        '(-inf,3.5.0)',
        '(-inf,3.5.1)',
        '[4.0.0,4.1.3)',
        '[5.0.0,5.6.1)',
        '[6.0.0,6.1.2)',
        '(-inf,3.7.0)',
        '(-inf,4.0.0)',
        '(-inf,4.17.5)',
        '(-inf,4.2.1)',
        '[5.0.0-alpha0,5.0.3)'
      ]

      target_versions = source_versions.flat_map do |item|
        SemverDialects::IntervalSetParser.translate_npm(item).map do |version_string|
          described_class.parse(typ, version_string).to_s
        end
      end

      expect(target_versions.to_set).to eq(expected_versions.to_set)
    end

    it 'parses intervals' do
      source_versions = [
        '>=2.1.4-M1 <2.1.5-M1',
        '<2.6.0',
        '>=3.0.0 <3.1.0',
        '<2.9.4',
        '<3.0.0',
        '<3.0.1',
        '<3.11.0',
        '>=4.0.0-rc1 <4.5.0',
        '<3.23.5',
        '<3.3.1',
        '<3.4.6',
        '>=4.0.0 <4.0.5',
        '<3.5.0',
        '<3.7.0',
        '<4.0.0',
        '<4.17.5',
        '=4.17.5'
      ]

      target_versions = source_versions.flat_map do |item|
        SemverDialects::IntervalSetParser.translate_npm(item).map do |version_string|
          described_class.parse(typ, version_string).to_npm_s
        end
      end

      expect(target_versions.to_set).to eq(source_versions.to_set)
    end
  end

  context 'with packagist syntax' do # rubocop:todo Metrics/BlockLength
    let(:typ) { 'packagist' }

    it 'parses intervals' do
      source_versions = [
        '<2.3.16||>=3.0.0-alpha,<3.0.10||>=3.1.0-alpha,<3.1.7||>=3.2.0-alpha,<3.2.7||>=3.3.0-alpha,<3.3.5',
        '<2.3.27||>=2.4.0-alpha,<2.5.11||>=2.6.0-alpha,<2.6.6',
        '<2.3.41||>=2.4.0-alpha,<2.7.13||>=2.8.0-alpha,<2.8.6||>=3.0.0-alpha,<3.0.6',
        '<2.4.11||>=2.5.0,<2.7.2',
        '<2.5.9||>=2.6.0,<2.6.11||>=2.7.0,<2.7.2',
        '<2.6.0',
        '<2.7.13||>=2.8.0-alpha,<2.8.6||>=3.0.0-alpha,<3.0.6',
        '<2.8.1||>=3.0.0a1,<3.0.0RC4',
        '<2.8.3',
        '<2017.08.0',
        '<3.1.14',
        '<3.1.16||>=3.2.0,<3.2.1',
        '<3.1.20||>=3.2.0-alpha0,<3.2.5||>=3.3.0-alpha0,<3.3.3||>=3.4.0-alpha0,<3.4.1',
        '<3.1.21',
        '<3.1.21||>=3.2.0-alpha0,<3.2.6||>=3.3.0-alpha0,<3.3.4||>=3.4.0-alpha0,<3.4.2',
        '<3.4.4-rc1||>=3.5.0-alpha0,<3.5.2-rc1',
        '<4.2.4||>=5.0.0,< 5.3.1||>6.0.0,<6.2.1',
        '<4.4.18||>=4.5.0-alpha0,<4.5.8',
        '<4.4.8||=4.5.0-beta1',
        '<5.5.40||>=5.6.0-alpha0,<5.6.15',
        '<6.2.27||>=7.0.0.alpha,<7.6.11||>=8.0.0.alpha,<8.3.1',
        '<6.2.29||>=7.6.0,<7.6.13||>=8.0.0,<8.4.1',
        '<8.4.7||>=8.5.0-alpha0,<8.5.2',
        '<8.4.8||>=8.5.0-alpha0,<8.5.3'
      ]

      expected_versions = [
        '(-inf,2.3.16)',
        '[3.0.0-alpha,3.0.10)',
        '[3.1.0-alpha,3.1.7)',
        '[3.2.0-alpha,3.2.7)',
        '[3.3.0-alpha,3.3.5)',
        '(-inf,2.3.27)',
        '[2.4.0-alpha,2.5.11)',
        '[2.6.0-alpha,2.6.6)',
        '(-inf,2.3.41)',
        '[2.4.0-alpha,2.7.13)',
        '[2.8.0-alpha,2.8.6)',
        '[3.0.0-alpha,3.0.6)',
        '(-inf,2.4.11)',
        '[2.5.0,2.7.2)',
        '(-inf,2.5.9)',
        '[2.6.0,2.6.11)',
        '[2.7.0,2.7.2)',
        '(-inf,2.6.0)',
        '(-inf,2.7.13)',
        '[2.8.0-alpha,2.8.6)',
        '[3.0.0-alpha,3.0.6)',
        '(-inf,2.8.1)',
        '[3.0.0a1,3.0.0RC4)',
        '(-inf,2.8.3)',
        '(-inf,2017.08.0)',
        '(-inf,3.1.14)',
        '(-inf,3.1.16)',
        '[3.2.0,3.2.1)',
        '(-inf,3.1.20)',
        '[3.2.0-alpha0,3.2.5)',
        '[3.3.0-alpha0,3.3.3)',
        '[3.4.0-alpha0,3.4.1)',
        '(-inf,3.1.21)',
        '(-inf,3.1.21)',
        '[3.2.0-alpha0,3.2.6)',
        '[3.3.0-alpha0,3.3.4)',
        '[3.4.0-alpha0,3.4.2)',
        '(-inf,3.4.4-rc1)',
        '[3.5.0-alpha0,3.5.2-rc1)',
        '(-inf,4.2.4)',
        '[5.0.0,5.3.1)',
        '(6.0.0,6.2.1)',
        '(-inf,4.4.18)',
        '[4.5.0-alpha0,4.5.8)',
        '(-inf,4.4.8)',
        '[4.5.0-beta1,4.5.0-beta1]',
        '(-inf,5.5.40)',
        '[5.6.0-alpha0,5.6.15)',
        '(-inf,6.2.27)',
        '[7.0.0.alpha,7.6.11)',
        '[8.0.0.alpha,8.3.1)',
        '(-inf,6.2.29)',
        '[7.6.0,7.6.13)',
        '[8.0.0,8.4.1)',
        '(-inf,8.4.7)',
        '[8.5.0-alpha0,8.5.2)',
        '(-inf,8.4.8)',
        '[8.5.0-alpha0,8.5.3)'
      ]

      target_versions = source_versions.flat_map do |item|
        SemverDialects::IntervalSetParser.translate_packagist(item).map do |version_string|
          described_class.parse(typ, version_string).to_s
        end
      end

      expect(target_versions.to_set).to eq(expected_versions.to_set)
    end

    it 'parses intervals' do
      source_versions = [
        '>=3.0.0-alpha',
        '>=3.0.0-alpha,<3.0.6',
        '>=2.5.0,<2.7.2',
        '>=2.6.0,<2.6.11',
        '>=2.7.0,<2.7.2',
        '<2.6.0',
        '>=3.0.0-alpha,<3.0.6',
        '>=3.0.0a1,<3.0.0RC4',
        '<2.8.3',
        '<2017.08.0',
        '<3.1.14',
        '>=3.2.0,<3.2.1',
        '>=8.5.0-alpha0,<8.5.2',
        '>=8.5.0-alpha0,<8.5.3'
      ]

      target_versions = source_versions.flat_map do |item|
        SemverDialects::IntervalSetParser.translate_packagist(item).map do |version_string|
          described_class.parse(typ, version_string).to_packagist_s
        end
      end

      expect(target_versions.to_set).to eq(source_versions.to_set)
    end
  end

  context 'with pypi syntax' do # rubocop:todo Metrics/BlockLength
    let(:typ) { 'pypi' }

    it 'parses intervals' do # rubocop:todo Metrics/BlockLength
      source_versions = [
        '<1.11.1 || >=1.12.0,<1.12.2 || >=1.13.0,<1.13.2 || >=1.14.0,<1.14.2',
        '<1.11.18 || >=2.0.0.dev0, <2.0.10 || >=2.1.0.dev0, <2.1.5',
        '<1.11.19 || >=2.0a1, <2.0.11 || >=2.1a1, <2.1.6 || >=2.2a1, <2.2b1',
        '<1.11.21 || >=2.0a1, <2.1.9 || >=2.2a1, <2.2.2',
        '<1.2.0',
        '<1.2.18 || >=1.3.0dev0,<1.3.0b3',
        '<1.2.6',
        '<1.7.0',
        '<1.7.11||>=1.8.0a,<1.8.7||>=1.9a,<1.9rc2',
        '<1.8.1',
        '<1.8.14||>=1.9.0alpha,<1.9.8||>=1.10.0alpha,<1.10rc1',
        '<1.8.2||>=2.0.0,<2.3.1',
        '<1.8.6||>=1.9.0alpha,<1.10.5||>=2.0.0alpha,<2.0.2',
        '<1.9.0',
        '<1.9.2',
        '<10.0.8 || >=11.0.0dev0, <11.0.7 || >=12.0.0dev0, <12.0.6 || >=13.0.0dev0',
        '<11.0.0',
        '<19.2.1',
        '<2.10.1',
        '<2.2.3',
        '<2.2.3 || >=2.3.0, <2.3.2',
        '<2.5.12 || >=2.6.0.dev0, <2.6.9 || >=2.7.0.dev0, <2.7.3',
        '<2.5.14 || >=2.6.0.dev0, <2.6.11 || >=2.7.0.dev0, <2.7.5',
        '<2.6.0',
        '<2.7.0',
        '<2.8',
        '<2.8.0',
        '<3.1.1',
        '<3.2.2||>=4.1,<4.2.0b1',
        '<4.2.5',
        '<4.3.12||>=5.0a1,<5.0.7',
        '<4.3.8||>=5.0a1,<5.0.1'
      ]

      expected_versions = [
        '(-inf,1.11.1)',
        '[1.12.0,1.12.2)',
        '[1.13.0,1.13.2)',
        '[1.14.0,1.14.2)',
        '(-inf,1.11.18)',
        '[2.0.0.dev0,2.0.10)',
        '[2.1.0.dev0,2.1.5)',
        '(-inf,1.11.19)',
        '[2.0a1,2.0.11)',
        '[2.1a1,2.1.6)',
        '[2.2a1,2.2b1)',
        '(-inf,1.11.21)',
        '[2.0a1,2.1.9)',
        '[2.2a1,2.2.2)',
        '(-inf,1.2.0)',
        '(-inf,1.2.18)',
        '[1.3.0dev0,1.3.0b3)',
        '(-inf,1.2.6)',
        '(-inf,1.7.0)',
        '(-inf,1.7.11)',
        '[1.8.0a,1.8.7)',
        '[1.9a,1.9rc2)',
        '(-inf,1.8.1)',
        '(-inf,1.8.14)',
        '[1.9.0alpha,1.9.8)',
        '[1.10.0alpha,1.10rc1)',
        '(-inf,1.8.2)',
        '[2.0.0,2.3.1)',
        '(-inf,1.8.6)',
        '[1.9.0alpha,1.10.5)',
        '[2.0.0alpha,2.0.2)',
        '(-inf,1.9.0)',
        '(-inf,1.9.2)',
        '(-inf,10.0.8)',
        '[11.0.0dev0,11.0.7)',
        '[12.0.0dev0,12.0.6)',
        '[13.0.0dev0,+inf)',
        '(-inf,11.0.0)',
        '(-inf,19.2.1)',
        '(-inf,2.10.1)',
        '(-inf,2.2.3)',
        '(-inf,2.2.3)',
        '[2.3.0,2.3.2)',
        '(-inf,2.5.12)',
        '[2.6.0.dev0,2.6.9)',
        '[2.7.0.dev0,2.7.3)',
        '(-inf,2.5.14)',
        '[2.6.0.dev0,2.6.11)',
        '[2.7.0.dev0,2.7.5)',
        '(-inf,2.6.0)',
        '(-inf,2.7.0)',
        '(-inf,2.8)',
        '(-inf,2.8.0)',
        '(-inf,3.1.1)',
        '(-inf,3.2.2)',
        '[4.1,4.2.0b1)',
        '(-inf,4.2.5)',
        '(-inf,4.3.12)',
        '[5.0a1,5.0.7)',
        '(-inf,4.3.8)',
        '[5.0a1,5.0.1)'
      ]

      target_versions = source_versions.flat_map do |item|
        SemverDialects::IntervalSetParser.translate_pypi(item).map do |version_string|
          described_class.parse(typ, version_string).to_s
        end
      end

      expect(target_versions.to_set).to eq(expected_versions.to_set)
    end

    it 'parses intervals' do
      source_versions = [
        '>=1.8.0a,<1.8.7',
        '>=1.9a,<1.9rc2',
        '<1.8.1',
        '>=2.0.0,<2.3.1',
        '>=1.9.0alpha,<1.10.5',
        '<1.9.0',
        '<1.9.2',
        '<11.0.0',
        '<3.1.1',
        '>=4.1,<4.2.0b1',
        '<4.2.5',
        '>=5.0a1,<5.0.7',
        '>=5.*,<5.0.1'
      ]

      target_versions = source_versions.flat_map do |item|
        SemverDialects::IntervalSetParser.translate_pypi(item).map do |version_string|
          described_class.parse(typ, version_string).to_pypi_s
        end
      end

      expect(target_versions.to_set).to eq(source_versions.to_set)
    end
  end

  context 'with go syntax' do
    let(:typ) { 'go' }

    it 'parses intervals' do
      source_versions = [
        '>=1.8.0-a <1.8.7',
        '<1.8.1',
        '>=2.0.0 <2.3.1',
        '>=1.9.0-alpha <1.10.5'
      ]

      target_versions = source_versions.flat_map do |item|
        SemverDialects::IntervalSetParser.translate_go(item).map do |version_string|
          described_class.parse(typ, version_string).to_go_s
        end
      end

      expect(target_versions.to_set).to eq(source_versions.to_set)
    end
  end

  context 'when the input contains an invalid version' do
    using RSpec::Parameterized::TableSyntax

    subject { described_class.parse(typ, input) }

    where(:typ, :input) do
      'gem'   | '>1.'
      'go'    | '>x.2'
      'maven' | '>1*'
      'maven' | '>=-'
      'npm'   | '>1a'
      'nuget' | '>1a'
      'gem'   | '<1.2.3.*'
      'pypi'  | '[,1.0.0]'
    end

    with_them do
      it 'raises an error' do
        expect do
          subject
        end.to raise_error(an_instance_of(SemverDialects::InvalidConstraintError))
      end
    end
  end
end
