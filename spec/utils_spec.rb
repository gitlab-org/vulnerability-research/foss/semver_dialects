# frozen_string_literal: true

RSpec.describe String do
  describe '#number?' do
    tests = [
      ['42', true],
      ['-41', true],
      ['+41', true],
      ['', false],
      ['v42', false]
    ]

    tests.each do |input, want|
      it_behaves_like 'table-driven test', input, want do
        subject { input.number? }
      end
    end
  end

  describe '#unquote' do
    tests = [
      ['"Hello"', 'Hello'],
      ['"Hello\'', 'Hello'],
      ['\'Hello"', 'Hello'],
      ["'Hello'", 'Hello'],
      ['Hello"', 'Hello']
    ]

    tests.each do |input, want|
      it_behaves_like 'table-driven test', input, want do
        subject { input.unquote }
      end
    end
  end

  describe '#csv_unquote' do
    it 'removes csv quotes' do
      expect('"""Hello"""'.csv_unquote).to eq('Hello')
    end
  end

  describe '#chars_only' do
    it 'removes not-printable characters' do
      expect('hello123:\-;*#@test'.chars_only).to eq('hello123test')
    end
  end
end
