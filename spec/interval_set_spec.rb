# frozen_string_literal: true

RSpec.describe SemverDialects::IntervalSet do
  context 'Semantic Version comparison' do
    it 'should work for pypi' do
      typ = 'pypi'
      version_string = '>=2.7.0dev0,<2.7.1 || >=2.6.0dev0,<2.6.7 || <2.5.11'
      version_array = SemverDialects::IntervalSetParser.translate_pypi(version_string)
      vr = described_class.new
      version_array.each do |item|
        interval = SemverDialects::IntervalParser.parse(typ, item)
        vr.add(interval)
      end

      expect(version_array.size).to eq(3)
    end

    it 'should work for maven' do
      typ = 'maven'
      version_string = '[8.5.0, 8.5.32), [9.0.0.M1, 9.0.10)'
      version_array = SemverDialects::IntervalSetParser.translate_maven(version_string)
      vr = described_class.new
      version_array.each do |item|
        interval = SemverDialects::IntervalParser.parse(typ, item)
        vr.add(interval)
      end

      expect(version_array.size).to eq(2)

      version_string = '(,3.1.13],3.2.0,[,]'
      version_array = SemverDialects::IntervalSetParser.translate_maven(version_string)
      vr = described_class.new
      version_array.each do |item|
        interval = SemverDialects::IntervalParser.parse(typ, item)
        vr.add(interval)
      end

      expect(version_array.size).to eq(3)
    end

    it 'should work for nuget' do
      typ = 'nuget'
      version_string = '[8.5.0, 8.5.32), [9.0.0-m1, 9.0.10)'
      version_array = SemverDialects::IntervalSetParser.translate_nuget(version_string)
      vr = described_class.new
      version_array.each do |item|
        interval = SemverDialects::IntervalParser.parse(typ, item)
        vr.add(interval)
      end

      expect(version_array.size).to eq(2)

      version_string = '(,3.1.13],3.2.0,[,]'
      version_array = SemverDialects::IntervalSetParser.translate_nuget(version_string)
      vr = described_class.new
      version_array.each do |item|
        interval = SemverDialects::IntervalParser.parse(typ, item)
        vr.add(interval)
      end

      expect(version_array.size).to eq(3)
    end

    it 'should work for packagist' do
      typ = 'packagist'
      version_string = '5.2.18||5.2.19'
      version_array = SemverDialects::IntervalSetParser.translate_packagist(version_string)
      vr = described_class.new
      version_array.each do |item|
        interval = SemverDialects::IntervalParser.parse(typ, item)
        vr.add(interval)
      end

      expect(version_array.size).to eq(2)
    end

    it 'should represent univeral ranges correcty' do
      typ = 'npm'
      version_array = ['=*']
      vr = described_class.new
      version_array.each do |item|
        interval = SemverDialects::IntervalParser.parse(typ, item)
        vr.add(interval)
      end

      expect(vr.universal?).to be(true)

      version_string = '>= 0.0.0'
      version_array = SemverDialects::IntervalSetParser.translate_packagist(version_string)
      vr = described_class.new
      version_array.each do |item|
        interval = SemverDialects::IntervalParser.parse(typ, item)
        vr.add(interval)
      end

      expect(vr.universal?).to be(true)
    end
  end
end
