# frozen_string_literal: true

RSpec.describe SemverDialects::IntervalSetParser do
  shared_examples 'an interval set parser' do |typ|
    it 'generates a set with the expected intervals' do
      want_intervals = want.map { |x| SemverDialects::IntervalParser.parse(typ, x) }
      got = described_class.parse(typ, input)

      expect(want_intervals).to eq(got.intervals)
    end
  end

  it_behaves_like 'an interval set parser', 'gem' do
    let(:input) { '>=2.3.0 <3.9.5 || >=3.10 <3.12 || >=4.0.0.preview2 <4.0.0.rc.2' }

    let(:want) do
      [
        '>=2.3.0 <3.9.5',
        '>=3.10 <3.12',
        '>=4.0.0.preview2 <4.0.0.rc.2'
      ]
    end
  end

  it_behaves_like 'an interval set parser', 'maven' do
    let(:input) do
      '(,3.2.9.RELEASE],[4.1-alpha0,4.1.3.RELEASE],' \
      '[4.2-alpha0,4.2.0.RELEASE],' \
      '(,6.6.6),[7.0.0,7.7.0),[1.7.5,1.7.5)'
    end

    let(:want) do
      [
        '<=3.2.9.RELEASE',
        '>=4.1-alpha0 <=4.1.3.RELEASE',
        '>=4.2-alpha0 <=4.2.0.RELEASE',
        '<6.6.6',
        '>=7.0.0 <7.7.0',
        '>=1.7.5 <1.7.5'
      ]
    end
  end

  it_behaves_like 'an interval set parser', 'npm' do
    let(:input) { '<3.5.1 || >=4.0.0 && <4.1.3 || >=5.0.0 && <5.6.1 || >=6.0.0 && <6.1.2' }

    let(:want) do
      [
        '<3.5.1',
        '>=4.0.0 <4.1.3',
        '>=5.0.0 <5.6.1',
        '>=6.0.0 <6.1.2'
      ]
    end
  end

  it_behaves_like 'an interval set parser', 'nuget' do
    let(:input) { '(,3.2.9],[4.1-alpha0,4.1.3],[4.2-alpha0,4.2.0],(,6.6.6),[7.0.0,7.7.0)' }

    let(:want) do
      [
        '<=3.2.9',
        '>=4.1-alpha0 <=4.1.3',
        '>=4.2-alpha0 <=4.2.0',
        '<6.6.6',
        '>=7.0.0 <7.7.0'
      ]
    end
  end

  it_behaves_like 'an interval set parser', 'packagist' do
    let(:input) do
      '<2.3.16||>=3.0.0-alpha,<3.0.10||>=3.1.0-alpha,<3.1.7' \
      '||>=3.2.0-alpha,<3.2.7||>=3.3.0-alpha,<3.3.5' \
      '||==2.4.0-rc0,==2.4.0-rc1,==2.4.0-rc2,==2.4.0-rc3,==2.4.0-rc4'
    end

    let(:want) do
      [
        '<2.3.16',
        '>=3.0.0-alpha <3.0.10',
        '>=3.1.0-alpha <3.1.7',
        '>=3.2.0-alpha <3.2.7',
        '>=3.3.0-alpha <3.3.5',
        '==2.4.0-rc0 ==2.4.0-rc1 ==2.4.0-rc2 ==2.4.0-rc3 ==2.4.0-rc4'
      ]
    end
  end

  it_behaves_like 'an interval set parser', 'pypi' do
    let(:input) do
      '<1.11.1 || >=1.12.0,<1.12.2 || >=1.13.0,<1.13.2 || >=1.14.0,<1.14.2'
    end

    let(:want) do
      [
        '<1.11.1',
        '>=1.12.0 <1.12.2',
        '>=1.13.0 <1.13.2',
        '>=1.14.0 <1.14.2'
      ]
    end
  end

  context 'when the package type is not supported' do
    it 'returns an exception' do
      expect { described_class.parse('unsupported-type', '<10.0.0') }
        .to raise_error(SemverDialects::UnsupportedPackageTypeError) { |error|
          expect(error.message).to include("unsupported package type 'unsupported-type'")
        }
    end
  end
end
