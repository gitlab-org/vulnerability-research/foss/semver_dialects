# frozen_string_literal: true

require './lib/semver_dialects/version'

GEMFILE_LOCK_VERSION_REGEX = /semver_dialects \((?<version>\d+\.\d+\.\d+)\)/
CHANGELOG_VERSION_REGEX = /## v(?<version>\d+\.\d+\.\d+)/

gemfile_lock = File.read('Gemfile.lock')
GEMFILE_LOCK_VERSION = gemfile_lock.match(GEMFILE_LOCK_VERSION_REGEX)['version']

changelog = File.read('CHANGELOG.md')
CHANGELOG_VERSION = changelog.match(CHANGELOG_VERSION_REGEX)['version']

if SemverDialects::VERSION != GEMFILE_LOCK_VERSION || SemverDialects::VERSION != CHANGELOG_VERSION
  puts 'Version drift detected!'
  puts ''
  puts '1. Ensure that lib/semver_dialects/version.rb and CHANGELOG.md match'
  puts '2. Run `bundler lock`'
  puts '3. Commit and push the latest changes'
  exit 1
end
