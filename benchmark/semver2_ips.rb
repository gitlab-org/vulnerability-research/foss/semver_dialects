# frozen_string_literal: true

require 'bundler/setup'
require 'semver_dialects'
require 'benchmark/ips'

Benchmark.ips do |x|
  [
    %w[release 1.2.3],
    %w[prerelease 1.2.3-alpha.1],
    %w[complex-prerelease 1.2.3-alpha.1.beta],
    %w[complex-prerelease-with-build-tag 1.2.3-alpha.1.beta+build1731]
  ].each do |(name, version)|
    x.report("semver2-#{name}") { Semver2::VersionParser.parse(version) }
    x.report("legacy-#{name}") { SemanticVersion.new(version) }
  end
end
