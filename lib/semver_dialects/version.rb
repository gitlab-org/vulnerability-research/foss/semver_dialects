# frozen_string_literal: true

module SemverDialects
  VERSION = '3.6.0'
end
