# semver_dialects Changelog

## v3.6.0
- Support versions and requirements of swift (!113)

## v3.5.0
- Add `sort` command (!112)
- Let unsupported types propagate to `parse_version` to maintain single source of truth (!112)

## v3.4.5
- Cleanup loose npm versions (!110)

## v3.4.4
- Fix parsing for invalid apk versions (!109)

## v3.4.3
- Fix parsing version pattern in Maven version (!108)
- Raise an error when an empty version is parsed (!108)

## v3.4.2
- Fix parsing of plus sign in Maven version (!104)

## v3.4.1
- Fix errors when parsing valid version strings that do not strictly adhere to SemVer (!101)

## v3.4.0
- Add support for `apk`, `deb` and `rpm` syntax to `SemverDialects.parse_version` (!99)

## v3.3.0
- Add support for apk (!97)

## v3.2.1
- Raise error for whitespace in RPM version strings (!95)

## v3.2.0
- Support versions and requirements of cargo (!93)

## v3.1.0
- Add support for rpm (!69)

## v3.0.2
- Compare boundaries using `<=>` and `Comparable` module (!84)

## v3.0.1
- Fix parsing of underscores in Maven version (!90)

## v3.0.0
- Rename `SemverDialects::VersionChecker.version_sat?` to `SemverDialects.version_satisfies?` (!79)
- Rename classes `VersionCut` to `Boundary` and `VersionRange` to `IntervalSet` (!54)
- New parser and comparison logic for Maven versions (!57, !59, and !67)
- New parser and comparison logic for npm, go, and nuget (!57, !58, and !68)
- Raise `IncompleteScanError` when a version string can't be fully scanned (!61)
- Move all modules and classes to `SemverDialects` namespace (!71)

## v2.0.2
- Improve `version_sat?` logic for disjunctions of equalities, enhancing accuracy in complex version constraints (!48)

## v2.0.1
- Fix comparison of versions that start with character `v` (!47)

## v2.0.0
- Raise specific errors when a version, a constraint, or a package type is invalid (!46)

## v1.6.2
- Move gem to new location and update URIs (!43)

## v1.6.1
- Resolve the `NoMethodError` that occurs when calling `VersionInterval#intersect` on an empty interval (!28)

## v1.6.0
- Improve performance of `String#number?` monkeypatch (!29)

## v1.5.1
- Raise `SemverDialects::Error` when version constraint strings do not contain a valid version specifier. (!20)

## v1.5.0
- Update `thor` from `1.2.2` to `1.3.0` (!19)

## v1.4.0
- Add support for `deb` versions (!16)

## v1.3.0
- Update from bundler `1.17.3` to `2.4.9` (!6)

## v1.2.1
- Fixes monkey patching String#present? method (!5)

## v1.2.0
- Dependency update pastel to 0.8.x from 0.7.x (!4)
- Dependency update tty-command to 0.10.1 from 0.9.0 (!4)

## v1.1.0
- Dependency update thor to 1.2.x (!3)

## v1.0.0
- Initial release
